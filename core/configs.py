from pydantic import BaseSettings
from sqlalchemy.ext.declarative import declarative_base


class Settings(BaseSettings):
    
    API_V1_STR: str = '/api/v1'
   
    DB_URL: str = "postgresql+asyncpg://ra22202037_user:4H75jPbteoOb8w9deYCABsBVXOaq6qxW@postgres.render.com:5432/ra22202037"
    DBBaseModel = declarative_base()

    class Config:
        case_sensitive = True


settings = Settings()
